bh_tusken_carnage_champion = Creature:new {
	objectName = "@mob/creature_names:tusken_fort_tusken_champion",
	socialGroup = "tusken_raider",
	faction = "tusken_raider",
	level = 116,
	chanceHit = 3.85,
	damageMin = 750,
	damageMax = 1210,
	baseXp = 11015,
	baseHAM = 43000,
	baseHAMmax = 53000,
	armor = 2,
	resists = {85,60,30,100,-1,100,-1,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/tusken_raider.iff"},
	lootGroups = {
		{
			groups = {
				{group = "purple_light_gem", chance = 500000},
				{group = "g_surging_necklace", chance = 500000},
				{group = "g_surging_ring", chance = 500000},
				{group = "color_crystals", chance = 2000000},
				{group = "power_crystals", chance = 1500000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000},
				{group = "g_ig88_loot_schem", chance = 1000000}
			},
			lootChance = 5320000
		},
		{
			groups = {
				{group = "purple_light_gem", chance = 1000000},
				{group = "g_surging_necklace", chance = 500000},
				{group = "g_surging_ring", chance = 1000000},
				{group = "color_crystals", chance = 2000000},
				{group = "power_crystals", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 1500000}
			},
			lootChance = 5320000
		},
		{
			groups = {
				{group = "purple_light_gem", chance = 500000},
				{group = "g_surging_necklace", chance = 500000},
				{group = "g_surging_ring", chance = 500000},
				{group = "g_ig_key", chance = 500000},
				{group = "color_crystals", chance = 2000000},
				{group = "power_crystals", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5320000
		},
	},
	weapons = {"tusken_weapons"},
	conversationTemplate = "",
	attacks = merge(marksmanmaster,brawlermaster,fencermaster,riflemanmaster)
}

CreatureTemplates:addCreatureTemplate(bh_tusken_carnage_champion, "bh_tusken_carnage_champion")