bh_force_crystal_hunter = Creature:new {
	objectName = "@mob/creature_names:dark_force_crystal_hunter",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "kun",
	faction = "",
	level = 115,
	chanceHit = 1,
	damageMin = 820,
	damageMax = 1350,
	baseXp = 10921,
	baseHAM = 24000,
	baseHAMmax = 30000,
	armor = 2,
	resists = {80,80,80,80,80,80,80,80,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dark_force_crystal_hunter.iff"},
	lootGroups = {
		{
			groups = {
				{group = "green_light_gem", chance = 1500000},
				{group = "g_tempest_ring", chance = 500000},
				{group = "g_blistering_ring", chance = 500000},
				{group = "g_blistering_necklace", chance = 500000},
				{group = "color_crystals", chance = 2000000},
				{group = "power_crystals", chance = 2000000},
				{group = "clothing_attachments", chance = 1500000},
				{group = "armor_attachments", chance = 1500000}
			},
			lootChance = 5300000
		},
		{
			groups = {
				{group = "green_light_gem", chance = 1000000},
				{group = "g_tempest_ring", chance = 500000},
				{group = "g_blistering_ring", chance = 500000},
				{group = "g_blistering_necklace", chance = 1000000},
				{group = "color_crystals", chance = 2000000},
				{group = "power_crystals", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 1000000}
			},
			lootChance = 5300000
		},
		{
			groups = {
				{group = "green_light_gem", chance = 500000},
				{group = "g_tempest_ring", chance = 500000},
				{group = "g_blistering_ring", chance = 500000},
				{group = "g_blistering_necklace", chance = 500000},
				{group = "g_exar_key", chance = 500000},
				{group = "color_crystals", chance = 2500000},
				{group = "power_crystals", chance = 2500000},
				{group = "clothing_attachments", chance = 1500000},
				{group = "armor_attachments", chance = 1000000}
			},
			lootChance = 5300000
		},
	},
	weapons = {"mixed_force_weapons"},
	conversationTemplate = "",
	attacks = merge(pikemanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(bh_force_crystal_hunter, "bh_force_crystal_hunter")