-- {"regionName", xCenter, yCenter, shape and size, tier, {"spawnGroup1", ...}, maxSpawnLimit}
-- Shape and size is a table with the following format depending on the shape of the area:
--   - Circle: {CIRCLE, radius}
--   - Rectangle: {2, width, height}
--   - Ring: {3, inner radius, outer radius}
-- Tier is a bit mask with the following possible values where each hexadecimal position is one possible configuration.
-- That means that it is not possible to have both a spawn area and a no spawn area in the same region, but
-- a spawn area that is also a no build zone is possible.

require("scripts.managers.spawn_manager.regions")

mandalore_regions = {
	{"world_spawner",0,0,{CIRCLE,-1},SPAWNAREA + WORLDSPAWNAREA + NOBUILDZONEAREA,{"mandalore_world"},2048},
	{"mand_imp_prison",-3670,-1113,{CIRCLE,500},NOSPAWNAREA + NOBUILDZONEAREA},
	{"enceri",3100,5280,{CIRCLE,500},NOSPAWNAREA + NOBUILDZONEAREA},
	{"norgbral",-6082,-2745,{CIRCLE,650},NOSPAWNAREA + NOBUILDZONEAREA},
	{"shuror",-2709,-3548,{CIRCLE,600},NOSPAWNAREA + NOBUILDZONEAREA},
	{"sundari",-5000,1000,{CIRCLE,600},NOSPAWNAREA + NOBUILDZONEAREA},
	{"keldabe",5200,-1200,{CIRCLE,600},NOSPAWNAREA + NOBUILDZONEAREA},
	{"mountaindungeon1",-2030,5450,{CIRCLE,500},NOSPAWNAREA + NOBUILDZONEAREA},
	{"mine_site",400,-6400,{CIRCLE,250},NOSPAWNAREA + NOBUILDZONEAREA},
	{"xman_01",-4900,-3750,{CIRCLE,250},NOSPAWNAREA + NOBUILDZONEAREA},
	{"xman_02",7025,6930,{CIRCLE,250},NOSPAWNAREA + NOBUILDZONEAREA},
	{"xman_03",1690,2270,{CIRCLE,250},NOSPAWNAREA + NOBUILDZONEAREA},



}
