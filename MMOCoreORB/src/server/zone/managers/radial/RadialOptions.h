/*
 * RadialOptions.h
 *
 *  Created on: 10/04/2010
 *      Author: victor
 */

#ifndef RADIALOPTIONS_H_
#define RADIALOPTIONS_H_

class RadialOptions {
public:
	const static int VEHICLE_GENERATE = 60;
	const static int VEHICLE_STORE = 61;
	const static int HYPERSPACE_MENU = 240;
	const static int HYPER_CPS = 241;
	const static int HYPER_TPS = 242;
	const static int HYPER_NOS = 243;
	const static int DUNGEON_MENU = 244;
	const static int DUNGEON_AVATAR = 245;
	const static int DUNGEON_EXAR = 246;
	const static int DUNGEON_AXKVA = 247;
	const static int DUNGEON_DDF = 248;
	const static int DUNGEON_WDF = 249;
	const static int DUNGEON_ORF = 250;
	const static int DUNGEON_HUTTBUNKER = 251;
	const static int DUNGEON_SHERKAR = 252;
	const static int DUNGEON_ISD = 253;
	const static int DUNGEON_IG = 254;
	const static int UNPACK_HOUSE = 63;
	const static int PAY_MAINT = 64;
	const static int CHECK_STATUS = 65;

};

#endif /* RADIALOPTIONS_H_ */
