/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.
*/

#include "server/zone/objects/intangible/ShipControlDevice.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/packets/object/ObjectMenuResponse.h"
#include "server/zone/Zone.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/packets/scene/PlayClientEffectLocMessage.h"
#include "templates/params/creature/CreatureFlag.h"
#include "server/zone/objects/region/CityRegion.h"

//#include "server/zone/objects/intangible/tasks/CallShipTask.h"
#include "server/zone/objects/intangible/tasks/CenterpointTravelTask.h"
#include "server/zone/objects/intangible/tasks/TansariiTravelTask.h"
#include "server/zone/objects/intangible/tasks/NovaOrionTravelTask.h"
#include "server/zone/objects/intangible/tasks/DungeonTravelTask.h"

#include "server/zone/objects/scene/SceneObject.h"

#include "server/zone/managers/radial/RadialOptions.h"


void ShipControlDeviceImplementation::playHyperCenterpointEffectTask(CreatureObject* player) {
		if (!player->checkCooldownRecovery("quickTravelCooldown")) {

			const Time* cdTime = player->getCooldownTime("quickTravelCooldown");
			int timeLeft = (floor((float)cdTime->miliDifference() / 1000) * -1) / 60;
			StringIdChatParameter stringId;

			if (timeLeft <= 1) {

			int timeLeftSeconds = (floor((float)cdTime->miliDifference() / 1000) * -1);

			stringId.setStringId("@skl_use:sys_quicktravel_delay_seconds"); // You must wait %DI seconds to call your ship again.			
			stringId.setDI(timeLeftSeconds);
			player->sendSystemMessage(stringId);

			return;
			} else {

			stringId.setStringId("@skl_use:sys_quicktravel_delay"); // You must wait %DI minutes to call your ship again.			
			stringId.setDI(timeLeft);
			player->sendSystemMessage(stringId);

			return;
			}
		}

		if (player->getParent() != nullptr || player->isInCombat()) {
			player->sendSystemMessage("You can only call a ship while outdoors and out of combat");
			return;
		}

		if (player->isIncapacitated() || player->isDead()) {
			player->sendSystemMessage("You cannot call a ship while dead or incapacitated");
			return;
		}

		PlayerObject* ghost = player->getPlayerObject();

		if (ghost->hasPvpTef() || ghost->hasBhTef()) {
			player->sendSystemMessage("You cannot call a ship while flagged!");
			return;
		}

		if (player->getCityRegion() != nullptr) {
			player->sendSystemMessage("You cannot call your ship in a municipal zone!");
			return;
		}

		if (player->isRidingMount()) {
			player->sendSystemMessage("You cannot call your ship while riding a mount!");
			return;
		}

		player->addCooldown("quickTravelCooldown", 300 * 1000); 

		ManagedReference<TangibleObject*> controlledObject = this->controlledObject.get();

		if (controlledObject->getServerObjectCRC() == 0xD1D6DA4A) {
			PlayClientEffectLoc* ArcLaunch = new PlayClientEffectLoc("clienteffect/arc170_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(ArcLaunch, true);
	
			//player->playEffect("clienteffect/arc170_landing.cef");
		}
		if (controlledObject->getServerObjectCRC() == 0x225C1758) {
			PlayClientEffectLoc* HavocLaunch = new PlayClientEffectLoc("clienteffect/havoc_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(HavocLaunch, true);
	
			//player->playEffect("clienteffect/havoc_landing.cef");
		}
		if (controlledObject->getServerObjectCRC() == 0x4735F720) {
		
			PlayClientEffectLoc* TwingLaunch = new PlayClientEffectLoc("clienteffect/twing_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(TwingLaunch, true);
	
			//player->playEffect("clienteffect/twing_landing.cef");

		}

		player->playMusicMessage("sound/veh_transport_landing.snd");

		Reference<CenterpointTravelTask*> centerpointTravel = new CenterpointTravelTask(_this.getReferenceUnsafeStaticCast(), player, "centerpoint_travel");

		player->addPendingTask("centerpoint_travel", centerpointTravel, 10 * 1000);

}

void ShipControlDeviceImplementation::playHyperTansariiEffectTask(CreatureObject* player) {
		if (!player->checkCooldownRecovery("quickTravelCooldown")) {

			const Time* cdTime = player->getCooldownTime("quickTravelCooldown");

			int timeLeft = (floor((float)cdTime->miliDifference() / 1000) * -1) / 60;

			StringIdChatParameter stringId;

			if (timeLeft <= 1) {

			int timeLeftSeconds = (floor((float)cdTime->miliDifference() / 1000) * -1);

			stringId.setStringId("@skl_use:sys_quicktravel_delay_seconds"); // You must wait %DI seconds to call your ship again.			
			stringId.setDI(timeLeftSeconds);
			player->sendSystemMessage(stringId);

			return;
			} else {

			stringId.setStringId("@skl_use:sys_quicktravel_delay"); // You must wait %DI minutes to call your ship again.			
			stringId.setDI(timeLeft);
			player->sendSystemMessage(stringId);

			return;
			}
		}

		if (player->isInCombat()) {
			player->sendSystemMessage("You cannot call a ship while in combat");
			return;
		}

		if (player->getParent() != nullptr || player->isInCombat()) {
			player->sendSystemMessage("You can only call a ship while outdoors and out of combat");
			return;
		}

		if (player->isIncapacitated() || player->isDead()) {
			player->sendSystemMessage("You cannot call a ship while dead or incapacitated");
			return;
		}

		PlayerObject* ghost = player->getPlayerObject();

		if (ghost->hasPvpTef() || ghost->hasBhTef()) {
			player->sendSystemMessage("You cannot call a ship while flagged!");
			return;
		}

		if (player->getCityRegion() != nullptr) {
			player->sendSystemMessage("You cannot call your ship in a municipal zone!");
			return;
		}

		if (player->isRidingMount()) {
			player->sendSystemMessage("You cannot call your ship while riding a mount!");
			return;
		}

		player->addCooldown("quickTravelCooldown", 300 * 1000); 

		ManagedReference<TangibleObject*> controlledObject = this->controlledObject.get();

		if (controlledObject->getServerObjectCRC() == 0xD1D6DA4A) {
			PlayClientEffectLoc* ArcLaunch = new PlayClientEffectLoc("clienteffect/arc170_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(ArcLaunch, true);
	
			//player->playEffect("clienteffect/arc170_landing.cef");
		}
		if (controlledObject->getServerObjectCRC() == 0x225C1758) {
			PlayClientEffectLoc* HavocLaunch = new PlayClientEffectLoc("clienteffect/havoc_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(HavocLaunch, true);
	
			//player->playEffect("clienteffect/havoc_landing.cef");
		}
		if (controlledObject->getServerObjectCRC() == 0x4735F720) {
		
			PlayClientEffectLoc* TwingLaunch = new PlayClientEffectLoc("clienteffect/twing_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(TwingLaunch, true);
	
			//player->playEffect("clienteffect/twing_landing.cef");

		}

		player->playMusicMessage("sound/veh_transport_landing.snd");

		Reference<TansariiTravelTask*> tansariiTravel = new TansariiTravelTask(_this.getReferenceUnsafeStaticCast(), player, "tansarii_travel");

		player->addPendingTask("tansarii_travel", tansariiTravel, 10 * 1000);

}

void ShipControlDeviceImplementation::playHyperNovaEffectTask(CreatureObject* player) {
		
		if (!player->checkCooldownRecovery("quickTravelCooldown")) {

			const Time* cdTime = player->getCooldownTime("quickTravelCooldown");

			int timeLeft = (floor((float)cdTime->miliDifference() / 1000) * -1) / 60;

			StringIdChatParameter stringId;

			if (timeLeft <= 1) {

			int timeLeftSeconds = (floor((float)cdTime->miliDifference() / 1000) * -1);
			
			stringId.setStringId("@skl_use:sys_quicktravel_delay_seconds"); // You must wait %DI seconds to call your ship again.			
			stringId.setDI(timeLeftSeconds);
			player->sendSystemMessage(stringId);

			return;
			} else {

			stringId.setStringId("@skl_use:sys_quicktravel_delay"); // You must wait %DI minutes to call your ship again.			
			stringId.setDI(timeLeft);
			player->sendSystemMessage(stringId);

			return;
			}
		}

		if (player->isInCombat()) {
			player->sendSystemMessage("You cannot call a ship while in combat");
			return;
		}

		if (player->getParent() != nullptr || player->isInCombat()) {
			player->sendSystemMessage("You can only call a ship while outdoors and out of combat");
			return;
		}

		if (player->isIncapacitated() || player->isDead()) {
			player->sendSystemMessage("You cannot call a ship while dead or incapacitated");
			return;
		}

		PlayerObject* ghost = player->getPlayerObject();

		if (ghost->hasPvpTef() || ghost->hasBhTef()) {
			player->sendSystemMessage("You cannot call a ship while flagged!");
			return;
		}

		if (player->getCityRegion() != nullptr) {
			player->sendSystemMessage("You cannot call your ship in a municipal zone!");
			return;
		}

		if (player->isRidingMount()) {
			player->sendSystemMessage("You cannot call your ship while riding a mount!");
			return;
		}

		player->addCooldown("quickTravelCooldown", 300 * 1000); 

		ManagedReference<TangibleObject*> controlledObject = this->controlledObject.get();

		if (controlledObject->getServerObjectCRC() == 0xD1D6DA4A) {
			PlayClientEffectLoc* ArcLaunch = new PlayClientEffectLoc("clienteffect/arc170_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(ArcLaunch, true);
	
			//player->playEffect("clienteffect/arc170_landing.cef");
		}
		if (controlledObject->getServerObjectCRC() == 0x225C1758) {
			PlayClientEffectLoc* HavocLaunch = new PlayClientEffectLoc("clienteffect/havoc_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(HavocLaunch, true);
	
			//player->playEffect("clienteffect/havoc_landing.cef");
		}
		if (controlledObject->getServerObjectCRC() == 0x4735F720) {
		
			PlayClientEffectLoc* TwingLaunch = new PlayClientEffectLoc("clienteffect/twing_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
			player->broadcastMessage(TwingLaunch, true);
	
			//player->playEffect("clienteffect/twing_landing.cef");

		}

		player->playMusicMessage("sound/veh_transport_landing.snd");

		Reference<NovaOrionTravelTask*> novaorionTravel = new NovaOrionTravelTask(_this.getReferenceUnsafeStaticCast(), player, "novaorion_travel");

		player->addPendingTask("novaorion_travel", novaorionTravel, 10 * 1000);

}

void ShipControlDeviceImplementation::playDungeonEffectTask(CreatureObject* player, byte selectedID) {
		
		if (!player->checkCooldownRecovery("dungeonTravelCooldown")) {

			const Time* cdTime = player->getCooldownTime("dungeonTravelCooldown");

			int timeLeft = (floor((float)cdTime->miliDifference() / 1000) * -1) / 60;

			StringIdChatParameter stringId;

			if (timeLeft <= 1) {

			int timeLeftSeconds = (floor((float)cdTime->miliDifference() / 1000) * -1);
			
			stringId.setStringId("@skl_use:sys_quicktravel_delay_seconds"); // You must wait %DI seconds to call your ship again.			
			stringId.setDI(timeLeftSeconds);
			player->sendSystemMessage(stringId);

			return;
			} else {

			stringId.setStringId("@skl_use:sys_quicktravel_delay"); // You must wait %DI minutes to call your ship again.			
			stringId.setDI(timeLeft);
			player->sendSystemMessage(stringId);

			return;
			}
		}

		if (player->isInCombat()) {
			player->sendSystemMessage("You cannot call a ship while in combat");
			return;
		}

		if (player->getParent() != nullptr || player->isInCombat()) {
			player->sendSystemMessage("You can only call a ship while outdoors and out of combat");
			return;
		}

		if (player->isIncapacitated() || player->isDead()) {
			player->sendSystemMessage("You cannot call a ship while dead or incapacitated");
			return;
		}

		PlayerObject* ghost = player->getPlayerObject();

		if (ghost->hasPvpTef() || ghost->hasBhTef()) {
			player->sendSystemMessage("You cannot call a ship while flagged!");
			return;
		}
/*
		if (player->getCityRegion() != nullptr) {
			player->sendSystemMessage("You cannot call your ship in a municipal zone!");
			return;
		}
*/
		if (player->isRidingMount()) {
			player->sendSystemMessage("You cannot call your ship while riding a mount!");
			return;
		}

		player->addCooldown("dungeonTravelCooldown", 21600 * 1000); 

		PlayClientEffectLoc* DungeonLaunch = new PlayClientEffectLoc("clienteffect/gunship_landing.cef", player->getZone()->getZoneName(), player->getPositionX(), player->getPositionZ() + 5, player->getPositionY());
		player->broadcastMessage(DungeonLaunch, true);	

		player->playMusicMessage("sound/veh_transport_landing.snd");

		if (selectedID == 245) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "avatar_travel");
		player->addPendingTask("avatar_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 246) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "exar_travel");
		player->addPendingTask("exar_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 247) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "axkva_travel");
		player->addPendingTask("axkva_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 248) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "ddf_travel");
		player->addPendingTask("ddf_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 249) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "wdf_travel");
		player->addPendingTask("wdf_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 250) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "orf_travel");
		player->addPendingTask("orf_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 251) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "huttbunker_travel");
		player->addPendingTask("huttbunker_travel", dungeonTravel, 10 * 1000);
		}

		if (selectedID == 252) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "sherkar_travel");
		player->addPendingTask("sherkar_travel", dungeonTravel, 10 * 1000);
		}

		//if (selectedID == 253) {
		//Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "isd_travel");
		//player->addPendingTask("isd_travel", dungeonTravel, 10 * 1000);
		//}

		if (selectedID == 254) {
		Reference<DungeonTravelTask*> dungeonTravel = new DungeonTravelTask(_this.getReferenceUnsafeStaticCast(), player, "ig_travel");
		player->addPendingTask("ig_travel", dungeonTravel, 10 * 1000);
		}

}

void ShipControlDeviceImplementation::fillObjectMenuResponse(ObjectMenuResponse* menuResponse, CreatureObject* player) {

	//ControlDeviceImplementation::fillObjectMenuResponse(menuResponse, player);

	ManagedReference<TangibleObject*> controlledObject = this->controlledObject.get();

	Zone* zone = player->getZone();

	PlayerObject* ghost = player->getPlayerObject();

	if (controlledObject->getServerObjectCRC() == 0x5ED88FE6) {

		menuResponse->addRadialMenuItem(244, 3, "Dungeon Quick Travel Locations");
		//String accessAvatar = ghost->getScreenPlayData("GlobalBadgeScript", "avatarTravelGranted");
		//if (accessAvatar == "1") {
		if (ghost->hasBadge(202)) {
			menuResponse->addRadialMenuItemToRadialID(244, 245, 3, "Avatar Station"); 
		}
		String accessExar = ghost->getScreenPlayData("GlobalBadgeScript", "exarTravelGranted");
		if (ghost->hasBadge(237)) {
			menuResponse->addRadialMenuItemToRadialID(244, 246, 3, "Exar Kun's Tomb"); 
		}
		//String accessAxkva = ghost->getScreenPlayData("GlobalBadgeScript", "axkvaTravelGranted");
		if (ghost->hasBadge(218))  {
			menuResponse->addRadialMenuItemToRadialID(244, 247, 3, "Axkva Min's Lair"); 
		}
		//String accessDDF = ghost->getScreenPlayData("GlobalBadgeScript", "ddfTravelGranted");
		if (ghost->hasBadge(214)) {
			menuResponse->addRadialMenuItemToRadialID(244, 248, 3, "Decrepit Droid Factory"); 
		}
		//String accessWDF = ghost->getScreenPlayData("GlobalBadgeScript", "wdfTravelGranted");
		if (ghost->hasBadge(215)) {
			menuResponse->addRadialMenuItemToRadialID(244, 249, 3, "Working Droid Factory"); 
		}
		//String accessORF = ghost->getScreenPlayData("GlobalBadgeScript", "orfTravelGranted");
		if (ghost->hasBadge(213)) {
			menuResponse->addRadialMenuItemToRadialID(244, 250, 3, "Old Research Facility"); 
		}
		//String accessHuttBunker = ghost->getScreenPlayData("GlobalBadgeScript", "huttbunkerTravelGranted");
		if (ghost->hasBadge(234)) {
			menuResponse->addRadialMenuItemToRadialID(244, 251, 3, "Plundered Gas Leak Bunker"); 
		}
		//String accessSherKar = ghost->getScreenPlayData("GlobalBadgeScript", "sherkarTravelGranted");
		if (ghost->hasBadge(210)) {
			menuResponse->addRadialMenuItemToRadialID(244, 252, 3, "Sher Kar's Cave"); 
		}
		//String accessISD = ghost->getScreenPlayData("GlobalBadgeScript", "isdTravelGranted");
		//if (accessISD == "1") {
		//	menuResponse->addRadialMenuItemToRadialID(244, 253, 3, "Imperial Star Destroyer"); 
		//}
		//String accessIG = ghost->getScreenPlayData("GlobalBadgeScript", "igTravelGranted");
		if (ghost->hasBadge(235)) {
			menuResponse->addRadialMenuItemToRadialID(244, 254, 3, "IG-88's Arena"); 
		}
	} else {

	//if (!zone->getZoneName().contains("space_")) {
//	if (!controlledObject->isInQuadTree()) {
//		menuResponse->addRadialMenuItem(60, 3, "Call Ship"); //Launch -
//	} else {
		menuResponse->addRadialMenuItem(240, 3, "Quick Travel Locations");
		menuResponse->addRadialMenuItemToRadialID(240, 241, 3, "Centerpoint Station");
		menuResponse->addRadialMenuItemToRadialID(240, 242, 3, "Tansarii Point Station");
		menuResponse->addRadialMenuItemToRadialID(240, 243, 3, "Nova Orion Station"); 
//	}	
	}
}

bool ShipControlDeviceImplementation::canBeTradedTo(CreatureObject* player, CreatureObject* receiver, int numberInTrade) {
	ManagedReference<SceneObject*> datapad = receiver->getSlottedObject("datapad");

	if (datapad == nullptr)
		return false;

	ManagedReference<PlayerManager*> playerManager = player->getZoneServer()->getPlayerManager();

	int shipsInDatapad = numberInTrade;
	int maxStoredShips = playerManager->getBaseStoredShips();

	for (int i = 0; i < datapad->getContainerObjectsSize(); i++) {
		Reference<SceneObject*> obj =  datapad->getContainerObject(i).castTo<SceneObject*>();

		if (obj != nullptr && obj->isShipControlDevice() ){
			shipsInDatapad++;
		}
	}

	if( shipsInDatapad >= maxStoredShips){
		player->sendSystemMessage("That person has too many ships in their datapad");
		receiver->sendSystemMessage("You already have the maximum number of ships that you can own.");
		return false;
	}

	return true;
}

