/*
 * based on CallMountTask.h
 *
 *  Created on: 1/22/2012
 *      Author: kyle
 */

#ifndef DUNGEONTRAVELTASK_H_
#define DUNGEONTRAVELTASK_H_

#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/intangible/ControlDevice.h"

class DungeonTravelTask : public Task {
	ManagedReference<CreatureObject*> player;
	ManagedReference<ShipControlDevice*> device;
	String taskName;

public:
	DungeonTravelTask(ShipControlDevice* controlDevice, CreatureObject* creo, const String& task) {
		player = creo;
		device = controlDevice;
		taskName = task;
	}

	void run() {

		if(player->isInCombat())
			return;

		if  (player->hasState(CreatureState::PILOTINGSHIP)) {		
		player->clearState(CreatureState::PILOTINGSHIP);
		}

		if  (player->hasState(CreatureState::SWIMMING)) {		
		player->clearState(CreatureState::SWIMMING);
		}	
		
		if (player->getPendingTask("avatar_travel") != nullptr) {
			player->removePendingTask("avatar_travel");
			player->switchZone("kashyyyk", 544.5, 31, 489.7, 0);
		}		
		
		if (player->getPendingTask("exar_travel") != nullptr) {
			player->removePendingTask("exar_travel");
			player->switchZone("yavin4", -2.3, -6.4, -1.8, 3465393);
		}		
		
		if (player->getPendingTask("axkva_travel") != nullptr) {
			player->removePendingTask("axkva_travel");
			player->switchZone("dathomir", -4176, 123, 20, 0);
		}		
		
		if (player->getPendingTask("ddf_travel") != nullptr) {
			player->removePendingTask("ddf_travel");
			player->switchZone("mustafar", 520, 65, 1986, 0);
		}			
		
		if (player->getPendingTask("wdf_travel") != nullptr) {
			player->removePendingTask("wdf_travel");
			player->switchZone("mustafar", 520, 65, 1986, 0);
		}			
		
		if (player->getPendingTask("orf_travel") != nullptr) {
			player->removePendingTask("orf_travel");
			player->switchZone("mustafar", -770, 87, 6084, 0);
		}			
		
		if (player->getPendingTask("huttbunker_travel") != nullptr) {
			player->removePendingTask("huttbunker_travel");
			player->switchZone("nalhutta", -3640, 24, -7016, 0);
		}			
		
		if (player->getPendingTask("sherkar_travel") != nullptr) {
			player->removePendingTask("sherkar_travel");
			player->switchZone("mustafar", -2005.8, 82.2, 4186.8, 0);
		}			
		
		//if (player->getPendingTask("isd_travel") != nullptr) {
		//	player->removePendingTask("isd_travel");
		//	player->switchZone("kashyyyk", 500, 0, 500, 0);
		//}		
		
		if (player->getPendingTask("ig_travel") != nullptr) {
			player->removePendingTask("ig_travel");
			player->switchZone("lok", 491.2, 12, 4832.2, 0);
		}		
		
	}
};

#endif /* DUNGEONTRAVELTASK_H_ */
